# -*- coding: utf-8 -*-

## Trabalho 6
## Daniel de Sousa da Silva
## DRE 118064962

import fileinput
import random

## Ex 1
for s in fileinput.input("Palavras.txt"):
    pass
print (fileinput.filelineno( ))

## Ex 2
size = 0
for s in fileinput.input("Palavras.txt"):
    s = s.strip()
    if (len(s) > size):
        size = len(s)
for s in fileinput.input("Palavras.txt"):
    s = s.strip()
    if (len(s) == size):
        print (s)

## Ex 3
for s in fileinput.input("Palavras.txt"):
    s = s.strip()
    if (len(s) >= 3 and s == s[::-1]):
        print (s)

## Ex 4
padrao = "S**V***R**"
for s in fileinput.input("Palavras.txt"):
    sucesso = 0
    s = s.strip()
    if (len(s) == len(padrao)):
        for i in range(0, len(s)):
            if (padrao[i] == "*" or padrao[i] == s[i]):
                sucesso += 1
        if (sucesso == len(s)):
            print(s)

## Desafio

# Seleciona palavra
def select():
    arquivo = fileinput.input("Palavras.txt")
    lista = []
    for i in arquivo:
        if(i > 4):
            lista.append(i)
    palavra = random.choice(lista).strip() #Palavra Secreta
    # String escondida
    linha = (len(palavra)*"*")

    logica(linha, palavra)
      
def logica(linha, palavra):
    certo = 0
    erros = "ERROS: "
    chances = 6
    digitados = []
    # Se não ganhou ou perdeu recebe entradas
    while(certo != len(palavra) and chances > 0):
        desenho(certo, erros, chances, palavra, linha)
        letra = raw_input("\nDigite uma letra >>> ").upper() # Entrada em Caixa-alta
        existe = False # Existência da letra na palavra
        
        while(letra in digitados):
            letra = raw_input("\nATENÇÃO: Letra repetida. \nDigite outra letra >>> ").upper()
        else:
            digitados.append(letra)
            
        # Verifica existência da letra na palavra
        if (letra not in palavra):
            chances -= 1 #Reduz chances
            if (chances == 5):
                erros +=  letra
            else:
                erros += ", " +  letra
        else:
             for i in range(0, len(palavra)):
                if (letra == palavra[i]):
                    linha = linha[:i] + letra.upper() +linha[i+1:]
                    certo += 1 # Conta os acertos
                    existe = True
                    
    desenho(certo, erros, chances, palavra, linha)
    
def desenho(certo, erros, chances, palavra, linha):
    print ("====================== FORCA ======================")
    print ("\n" + linha + " " + "\n" \
          +(10*" ") + erros)

    # Determina um vencedor
    if (certo == len(palavra)):
        print ("\nPARABÉNS, VOCÊ VENCEU!")
        return
    # Desenha na Tela
    if (certo != len(palavra)):
        print ("===========  \n"\
               +"         || \n"\
               +"         ||   ")
    if (chances == 5):
        print ("          0")
    elif (chances == 4):
        print ("          0 \n"\
              +"          | \n")
    elif (chances == 3):
        print ("          0 \n"\
              +"         /| \n")
    elif (chances == 2):
        print ("          0  \n"\
              +"         /|\ \n")
    elif (chances == 1):
        print ("          0  \n" \
              +"         /|\ \n" \
              +"         /   \n" )
    elif (chances == 0):
        print ("          0   \n"\
              +"         /|\  \n"\
              +"         / \  \n")
        print ("\nVOCÊ PERDEU! :(")
        print ("A PALAVRA ERA " + palavra)
            
select()
